import java.util.*;
public class ExcelColumnNumber {
	public int titleToNumber(String a) {
	    int num = 0;
	    for (int i = a.length() - 1, j = 0; i >= 0; i--) {
	        num += (int) Math.pow(26, j) * (a.charAt(i) - 'A' + 1);
	        j++;
	    }
	    return num;
	}
	public static void main(String args[])
	{
		ExcelColumnNumber ex = new ExcelColumnNumber();
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		int val = ex.titleToNumber(s);
		System.out.println(val);
	}
}