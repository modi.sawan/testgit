class BubbleSort
{
    public static void main(String args[])
    {
	int temp;
	int bs[] = {5,4,3,2};
	int n = 4;
	for(int i=0; i<(n-1); i++)
	{
		for(int j=0;j<(n-i-1);j++)
		{
			 if(bs[j]>bs[j+1])
			{
				temp = bs[j];
				bs[j] = bs[j+1];
				bs[j+1] = temp;
			}
		}
	}
	for(int c : bs)
	{ System.out.print(c); }
    }
}